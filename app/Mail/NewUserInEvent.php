<?php

namespace App\Mail;

use App\Event;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewUserInEvent extends Mailable
{
    use Queueable, SerializesModels;

    public $event;
    public $codigoqr;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    // por aqui a traves dek evento recibiremoas la info - 1.1 para mandarselo al
    // controlador oara que lo escuche y lo reciba en mi EventsController
    public function __construct(Event $event,$codigoqr)
    {
        //
        $this->event = $event;
        $this->codigoqr = $codigoqr;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject(__("Gracias por inscribirte al evento"))
            ->markdown('emails.new_user_inscrito')
            ->with('event',$this->event)
            ->with('codigoqr',$this->codigoqr);
    }
}
