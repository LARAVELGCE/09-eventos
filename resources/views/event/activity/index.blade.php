<!--Event Schedule Area Start Here-->
<div class="event-schedule-area-two bg-color pad100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title text-center">
                    <div class="title-text">
                        <h2>Cronograma de Actividades</h2>
                    </div>
                    <p>A continuacion le presentamos  las siguientes actividades pactadas y establecidas que se van
                        a llevar conjuntamente con los ponentes, el tema y  la duracion  .</p>
                </div>
            </div>
            <!-- /.col end-->
        </div>
        <!-- row end-->
        <div class="row">
            <div class="col-lg-12">

                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="text-center" scope="col">Date</th>
                                    <th scope="col">Speakers</th>
                                    <th scope="col">Session</th>
                                    <th scope="col">Description</th>
                                    <th class="text-center" scope="col">More</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($event->activities as $activity)
                                    <tr class="inner-box">
                                        <th scope="row">
                                            <div class="event-date">
                                                <span>{{ $activity->start_at->format('d') }}</span>
                                                <p>{{ $activity->start_at->format('M') }}</p>
                                            </div>
                                        </th>
                                        <td>

                                        <div class="event-img">
                                            <img src="{{ $activity->speaker->pathAttachment() }}" alt="">
                                        </div>

                                        </td>
                                        <td>
                                            <div class="event-wrap">
                                                <h3><a href="">{{ $activity->speaker->name }}</a></h3>
                                                <div class="meta">
                                                    <div class="organizers">
                                                        <a href="#">{{ $activity->speaker->job }}</a>
                                                    </div>

                                                    <div class="time">
                                                        <span>{{ $activity->start_at->format('g:ia') }} - {{ $activity->end_at->format('g:ia') }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="r-no">
                                                <span>{{ substr($activity->description, 0,30)}} ...</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="primary-btn">
                                                <a class="btn-primary" href="#">Leer Más</a>
                                            </div>
                                        </td>
                                    </tr>
                                @empty

                                @endforelse


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="primary-btn text-center">
                    <a href="#" class="btn-primary">Download Schedule</a>
                </div>
            </div>
            <!-- /col end-->
        </div>
        <!-- /row end-->
    </div>
    <!-- /container end-->
</div>
<!--Event Schedule Area End Here-->
