<?php

namespace App\Policies;

use App\User;
use App\Event;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventPolicy
{
    use HandlesAuthorization;


    public function inscribe (User $user, Event $event) {
        // si el usuario se puede inscribir al evento
        // Comprobar si dentro de la relacion de muchos a muchos
        // si el usuario ya esta escrito en el evento
        return ! $event->users->contains($user->id);
    }


}
