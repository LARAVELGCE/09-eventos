<!--Whos Speaking Area Start Here-->
<div class="whos-speaking-area pad100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title text-center">
                    <div class="title-text mb50">
                        <h2>Who's Speaking?</h2>
                    </div>
                </div>
            </div>
            <!-- /col end-->
        </div>
        <!-- /.row  end-->
        <div class="row mb50">
            @forelse ($event->activities as $activity)
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12">
                    <div class="speakers xs-mb30">
                        <div class="spk-img">
                            <img class="img-fluid" src="{{ $activity->speaker->pathAttachment() }}" alt="trainer-img">
                            <ul>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-camera"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-life-ring"></i></a></li>
                            </ul>
                        </div>
                        <div class="spk-info">
                            <a href="speakers-single.html">
                                <h3>{{ $activity->speaker->name }}</h3>
                            </a>
                            <p>{{ $activity->speaker->job }}</p>
                        </div>
                    </div>
                </div>
            @empty

            @endforelse

            <!-- /col end-->
           {{-- <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12">
                <div class="speakers xs-mb30">
                    <div class="spk-img">
                        <img class="img-fluid" src="/img/team/2.jpg" alt="trainer-img">
                        <ul>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-camera"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-life-ring"></i></a></li>
                        </ul>
                    </div>
                    <div class="spk-info">
                        <a href="speakers-single.html">
                            <h3>Toni Duggan</h3>
                        </a>
                        <p>GM, Pixelperfect</p>
                    </div>
                </div>
            </div>
            <!-- /col end-->
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12">
                <div class="speakers xs-mb30">
                    <div class="spk-img">
                        <img class="img-fluid" src="/img/team/3.jpg" alt="trainer-img">
                        <ul>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-camera"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-life-ring"></i></a></li>
                        </ul>
                    </div>
                    <div class="spk-info">
                        <a href="speakers-single.html">
                            <h3>Philipp Lahm</h3>
                        </a>
                        <p>Digital photography</p>
                    </div>
                </div>
            </div>
            <!-- /col end-->
            <div class="col-xl-3 col-lg-3 d-md-none d-lg-block col-sm-12">
                <div class="speakers">
                    <div class="spk-img">
                        <img class="img-fluid" src="/img/team/4.jpg" alt="trainer-img">
                        <ul>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-camera"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-life-ring"></i></a></li>
                        </ul>
                    </div>
                    <div class="spk-info">
                        <a href="speakers-single.html">
                            <h3>Lieke Martens</h3>
                        </a>
                        <p>CEO, Animation Studios</p>
                    </div>
                </div>
            </div>--}}
            <!-- /col end-->
        </div>
        <!-- /row end-->
        {{--<div class="row">
            <div class="offset-2 no-gutter"></div>
            <!-- /col end-->
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12">
                <div class="speakers xs-mb30">
                    <div class="spk-img">
                        <img class="img-fluid" src="/img/team/5.jpg" alt="trainer-img">
                        <ul>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-camera"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-life-ring"></i></a></li>
                        </ul>
                    </div>
                    <div class="spk-info">
                        <a href="speakers-single.html">
                            <h3>Fara Williams</h3>
                        </a>
                        <p>Designer, Treehouse</p>
                    </div>
                </div>
            </div>
            <!-- /col end-->
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12">
                <div class="speakers xs-mb30">
                    <div class="spk-img">
                        <img class="img-fluid" src="/img/team/6.jpg" alt="trainer-img">
                        <ul>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-camera"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-life-ring"></i></a></li>
                        </ul>
                    </div>
                    <div class="spk-info">
                        <a href="speakers-single.html">
                            <h3>Manuel Neuer</h3>
                        </a>
                        <p>CEO, Spingboard</p>
                    </div>
                </div>
            </div>
            <!-- /col end-->
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12">
                <div class="speakers">
                    <div class="spk-img">
                        <img class="img-fluid" src="/img/team/7.jpg" alt="trainer-img">
                        <ul>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-camera"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-life-ring"></i></a></li>
                        </ul>
                    </div>
                    <div class="spk-info">
                        <a href="speakers-single.html">
                            <h3>Lieke Martens</h3>
                        </a>
                        <p>Digital photography</p>
                    </div>
                </div>
            </div>
            <!-- /col end-->
        </div>--}}
        <!-- /row end-->
    </div>
    <!-- /container end-->
</div>
<!--Whos Speaking Area End Here-->