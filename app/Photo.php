<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Photo
 *
 * @property int $id
 * @property int $event_id
 * @property string|null $picture
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Event $event
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Photo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Photo whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Photo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Photo wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Photo whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $featured
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Photo whereFeatured($value)
 */
class Photo extends Model
{
    // Cada photo pertenece a un evento
    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function pathAttachment () {
        return "/images/events/" . $this->picture;
    }



}
