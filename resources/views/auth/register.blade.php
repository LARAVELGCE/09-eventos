@extends('layout')
@section('header-class','base-style-2')
@section('img','logo.png')

@section('content')
    <div class="hero-banner-area home-4  hero-bg-3 parallax no-attm">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-5 d-none d-lg-block">
                    <div class="banner-content">
                        <h1>Freelancers</h1>
                        <h2>Conferences 2018</h2>
                        <p>23-26 December 2020 with over 70 sessions - San Fancisco, CA</p>
                    </div>
                </div>
                <!-- /col end-->
                <div class="col-lg-5">
                    <div class="contact-box">
                        <div class="inner-content">
                            <h3>Registration Eventex</h3>
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="row">

                                    <div class="col-lg-12">
                                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"  name="name" value="{{ old('name') }}" placeholder="Name" >
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-lg-12">
                                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"  name="email" value="{{ old('email') }}" placeholder="E-mail" >
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-lg-12">
                                        <input  type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" >
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="col-lg-12">
                                        <input id="password-confirm"  type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"  name="password_confirmation" placeholder="Confirm Password" >

                                    </div>

                                    <div class="col-lg-12">
                                        <button class="btn-primary" type="submit">Registrarse Ahora</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /col end-->
            </div>
            <!-- /row end-->
        </div>
        <!-- /container end-->
    </div>

@endsection
