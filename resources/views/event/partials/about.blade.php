<!--Conference Synopsis Area Start Here-->
<div class="conference-synopsis-area pad80">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 xs-mb40">
                <div class="conference-active owl-carousel owl-theme">
                    @foreach ($event->photos as $photo)
                        <div class="inner-img xs-mb30 xs-mt30">
                            <img class="img-fluid" src="{{ $photo->pathAttachment() }}" alt="">
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- /.col end-->
            <div class="col-lg-7" style=" display: flex;align-items: center;">
                <div class="inner-content">
                    <div class="section-title">
                        <div class="title-text pl">
                            <h2>{{ $event->title }}</h2>
                        </div>
                    </div>
                    <p>{{ $event->description }},
                    </p>


                </div>
            </div>
            <!-- /col end-->
        </div>
        <!-- /row end-->
    </div>
    <!-- /container end-->
</div>
<!--Conference Synopsis Area End Here-->