@auth
    @can('inscribe', $event)
        <div class="primary-btn text-center mt-3">
            <a href="{{ route('events.inscribe', ['slug' => $event->slug]) }}" class="btn-primary btn_eventex">
                <i class="fa fa-user space_icon"></i>Inscribirme</a>
        </div>
    @else
        <div class="primary-btn text-center mt-3">
            <a href="#" class="btn-primary btn_eventex">
                <i class="fa fa-user space_icon"></i>Inscrito</a>
        </div>
    @endcan
@else
    <div class="primary-btn text-center mt-3">
        <a href="{{ route('login') }}" class="btn-primary btn_eventex">
            <i class="fa fa-user space_icon"></i>Acceder</a>
    </div>
@endauth
