<div class="col-xl-4 col-lg-4 col-md-12 col-sm-4">
    <div class="left-sidebar">
        <div class="sidebar-widget">
            <div class="search-box">
                <form action="#">
                    <input type="text" placeholder="Search" required>
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
        <div class="sidebar-widget">
            <div class="sidebar-title">
                <h4>CATEGORIAS</h4>
            </div>
            <div class="inner-box">
                <ul>
                    @forelse ($categories as $category)
                        <li><a href="{{ route('search.category', $category) }}">{{ $category->name }}</a> <span class="float-right">{{ $category->events_count }}</span></li>
                    @empty
                        No hay Categorias
                    @endforelse
                 </ul>

            </div>
        </div>
        <div class="sidebar-widget">
            <div class="sidebar-title">
                <h4>RECENT NEWS</h4>
            </div>
            @forelse ($eventex as $event)
                <div class="single-post">
                    <div class="post-thumb">
                        <img src="/images/events/{{ $event->photos->first()->picture }}" alt="">
                    </div>
                    <div class="post-content">
                        <div class="date">
                            <a href="#">{{ $event->start_at->format('d M Y') }}</a>
                        </div>
                        <p>{{  $event->title }} </p>
                    </div>
                </div>
            @empty
                No hay Eventos
            @endforelse

        </div>
        <div class="sidebar-widget mb-0">
            <div class="sidebar-title">
                <h4>Tag</h4>
            </div>
            <div class="popular-tag">
                <ul>
                    @forelse ($tags as $tag)
                        <li><a href="{{ route('search.tag', $tag) }}">{{ $tag->name }}</a></li>
                    @empty

                    @endforelse

                 {{--   <li><a href="#">Success</a></li>
                    <li><a href="#" class="mb-0">Client Meetings</a></li>
                    <li><a href="#" class="mb-0 mr-0">Charity</a></li>--}}
                </ul>
            </div>
        </div>
    </div>
</div>