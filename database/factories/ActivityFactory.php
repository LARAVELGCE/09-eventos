<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(App\Activity::class, function (Faker $faker) {
    $startDate = Carbon::createFromTimeStamp($faker->dateTimeBetween('-30 days', '+30 days')->getTimestamp());
    $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $startDate)->addHour();
    return [
        'event_id' => \App\Event::all()->random()->id,
        'speaker_id' => \App\Speaker::all()->random()->id,
        'description' =>  $faker->sentence,
        'start_at' =>  $startDate,
        'end_at' =>  $endDate,
    ];
});
