{{--Aqui creamos el markdow y con #h1 empezaos el mensaje --}}
@component('mail::message')
# {{ __("Eventex te da la bienvenia") }}

{{ __("FELICIDADES te has escrito al evento :event, ", ['event' => $event->title]) }} <br />
{{ __("Tu codigo QR  es :codigoqr", ['codigoqr' => $codigoqr]) }}

{!! QrCode::size(500)->generate($codigoqr); !!}




@endcomponent

