<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Event
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property string $location
 * @property string|null $slug
 * @property int|null $category_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Activity[] $activities
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Photo[] $photos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Sponsor[] $sponsors
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Tag[] $tags
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $featured_img_picture
 * @property string|null $start_at
 * @property-read \App\Category|null $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event published()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereStartAt($value)
 */
class Event extends Model
{
    // para que laravel se de cuenta que tambien tiene formato de fecha
    protected $dates = ['start_at'];

     protected $withCount = ['activities','sponsors'];

    // cada evento tiene muchas fotos
    public function photos() {
        return $this->hasMany(Photo::class);
    }

    // cada evento tiene muchas fotos
    public function sponsors() {
        return $this->hasMany(Sponsor::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    // cada evento tiene muchas fotos
    public function activities() {
        return $this->hasMany(Activity::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }



    public function scopePublished($query)
    {
        $query->with(['category','tags','photos'])
            ->whereNotNull('start_at')
            ->whereBetween('start_at', array(Carbon::now(), Carbon::now()->addMonths(1)))
            ->oldest('start_at');
    }

    public function scopeRecent($query)
    {
        $query->with(['category','tags','photos'])
            ->whereNotNull('created_at')
            ->latest('created_at');
    }


    // Slug para redirigit al event
    public function getRouteKeyName() {
        return 'slug';
    }

    public function relatedEvents () {
        return Event::with('photos')->whereCategoryId($this->category->id)
            ->where('id', '!=', $this->id)
            ->latest()
            ->limit(6)
            ->get();
    }





}
