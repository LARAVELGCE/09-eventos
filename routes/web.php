<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index')->name('pages.home');

// Ruta para la obtener la ruta la imagen
Route::get('/images/{path}/{attachment}', function($path, $attachment) {
    $file = sprintf('storage/%s/%s', $path, $attachment);
    // storage/%s/%s => como vemos se genera  en public/store/courses|%s|$path/imagenes|%s|$attachment
    if(File::exists($file)) {
        return Image::make($file)->response();
    }
});

// Buscar por categoria
Route::get('/categorias/{category}', 'CategoriesController@searchCategory')->name('search.category');
// Buscar por etiquetas - tags esta es la url
Route::get('/tags/{tag}', 'TagsController@searchTag')->name('search.tag');

Route::group(['prefix' => 'events'], function () {

    Route::group(['middleware' => ['auth']], function() {
        Route::get('/{event}/inscribe', 'EventsController@inscribe')->name('events.inscribe');
        Route::get('/create', 'EventsController@create')->name('events.create');
        Route::post('/store', 'EventsController@store')->name('events.store');
    });
    Route::get('/{event}', 'EventsController@show')->name('events.show');
});




//Route::get('/home', 'HomeController@index')->name('home');
