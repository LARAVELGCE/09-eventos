<!--Counter Up Area Start Here-->
<div class="counter-up-area pad100 bg-counter parallax">
    <div class="container">
        <div class="row">
            <!-- /col end-->
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                <div class="single-counter xs-mb40">
                    <div class="count-content">
                        <span class="count">{{ $event->activities_count }}</span>
                        <p>Actividades</p>
                    </div>
                </div>
            </div>
            <!-- /col end-->
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                <div class="single-counter xs-mb40">
                    <div class="count-content">
                        <span class="count">65</span>
                        <p>Speakers</p>
                    </div>
                </div>
            </div>
            <!-- /col end-->
            <div class="col-xl-4 col-lg-4 d-md-none d-lg-block col-sm-4">
                <div class="single-counter">
                    <div class="count-content">
                        <span class="count">{{ $event->sponsors_count }}</span>
                        <p>Sponsors</p>
                    </div>
                </div>
            </div>
            <!-- /col end-->
        </div>
        <!-- /row end-->
    </div>
    <!-- /container end-->
</div>
<!--Counter Up Area End Here-->