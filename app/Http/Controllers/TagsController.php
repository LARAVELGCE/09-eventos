<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    public function searchTag(Tag $tag)
    {
        // Estamos enviado a pantala del welcome solo las post de dicha categoria
        return view('events.home',
            [
                // Estamos enviado el tag
                'tag' => $tag,
                // Estamos enviado a pantala del welcome solo las post de dicha categoria
                'events' => $tag->events()->paginate(4)
            ]);
    }

}
