<!--Header Start Here-->
<header class="@yield('header-class') header">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <div class="brand-logo">
                    <a class="eventex-brand" href="{{ route('pages.home') }}"><img src="/img/logo/@yield('img')" alt=""></a>
                </div>
            </div>
            <!-- /col end-->
            <div class="col-lg-9">
                <nav class="navbar navbar-expand-lg">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"><i class="fa fa-bars"></i></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            @guest
                                <li class="nav-item {{ setActiveRoute('login') }}">
                                    <a class="nav-link" href="{{ route('login') }}">Ingresar</a>
                                </li>
                                <li class="nav-item {{ setActiveRoute('register') }}">
                                    <a class="nav-link" href="{{ route('register') }}">Registrarse</a>
                                </li>
                            @else
                                <li class="nav-item  dropdown">
                                    <a class="nav-link dropdown-toggle" href="#"> {{ Auth::user()->name }}  <span class="sr-only">(current)</span></a>
                                    <ul class="dropdown-menu animation  slideUpIn">
                                        <li>
                                            <a href="{{ route('events.create') }}">Crear Evento</a>
                                        </li>

                                        <li>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">   {{ __('Cerrar Sesión') }}</a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            @endguest

                        </ul>
                    </div>
                </nav>
            </div>
            <!-- /col end-->

            <!-- /col end-->
        </div>
        <!-- /row end-->
    </div>
    <!-- container end-->
</header>
<!--Header End Here-->
