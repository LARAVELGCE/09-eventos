<?php

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Iniciar y eliminar para volver a ejecutar los events
        Storage::deleteDirectory('events');
        Storage::deleteDirectory('sponsors');
        Storage::deleteDirectory('speakers');

        Storage::makeDirectory('events');
        Storage::makeDirectory('sponsors');
        Storage::makeDirectory('speakers');

        $this->call(CategoriesTableSeeder::class);

        factory(\App\Speaker::class,10)->create();

        $admin= new User();
        $admin->name = 'Geancarlos CE';
        $admin->email = 'gean@ed.team';
        $admin->password = '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm'; // secret
        $admin->save();
        factory(\App\User::class,10)->create();


        factory(\App\Event::class,15)
            ->create()
            ->each(function (\App\Event $e) {
                $e->photos()->saveMany(factory(\App\Photo::class,3)->create());
                $e->sponsors()->saveMany(factory(\App\Sponsor::class,6)->create());
                $e->activities()->saveMany(factory(\App\Activity::class,4)->create()
                );
            });



        $event = App\Event::all();
        //  Cada Usuario tiene de 2 a 5 evento
        App\User::all()->each(function ($user) use ($event) {
            $number = rand(2, 5);
            for($i = 0; $i < $number; $i++) {
                $user->events()->attach(
                    $event->random(1)->pluck('id')->toArray(),
                    ['codigo' => str_random(8)]
                );
            }

        });





        $this->call(TagsTableSeeder::class);

        $tag = App\Tag::all();

           //  Cada evento tiene de 2 a 5 tags
           App\Event::all()->each(function ($event) use ($tag) {
               $event->tags()->attach(
                   $tag->random(rand(2,5))->pluck('id')->toArray()

               );
           });




    }
}
