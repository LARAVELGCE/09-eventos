<?php

namespace App\Http\ViewComposers;

use App\Event;
use App\Tag;
use Illuminate\View\View;
use App\Category;


class AsideComposer
{
    public function compose(View $view)
    {

        $categories = Category::orderBy('name','asc')->withCount(['events'])->get();
        $tags = Tag::orderBy('name','asc')->get();

        $query = Event::recent();
        $eventex = $query->take(3)->get();

        $view->with('categories',$categories)
             ->with('tags',$tags)
             ->with('eventex',$eventex);

    }
}



