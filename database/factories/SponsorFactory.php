<?php

use Faker\Generator as Faker;

$factory->define(App\Sponsor::class, function (Faker $faker) {
    return [
        'event_id' => \App\Event::all()->random()->id,
        'picture' => \Faker\Provider\Image::image(storage_path() . '/app/public/sponsors', 200, 70, 'sponsors',false),
    ];
});
