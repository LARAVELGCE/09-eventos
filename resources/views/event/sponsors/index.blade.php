<!--Our Sponsers Area Start Here-->
<div class="our-sponsers-area-tow pad100 bg-color">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title text-center">
                    <div class="title-text mb50 xs-mb40">
                        <h2>Our Sponsers</h2>
                        <p>Ocurreret temporibus nec ad. Vim dolor appetere percipitur te. Illud noluisse petentium at mea,<br>pro vide eloquentiam ex, ne eum sumo aperiam. Hinc disputando an qui, no sed.</p>
                    </div>
                </div>
            </div>
            <!-- /col end-->
        </div>
        <!-- /row end-->
        <div class="row">
            <div class="sponsers-active owl-carousel owl-theme">
                @forelse ($event->sponsors as $sponsor)
                    <div class="col-lg-12">
                        <div class="single-sponsers">
                            <a href="#"><img src="{{ $sponsor->pathAttachment() }}" alt=""></a>
                        </div>
                    </div>
                @empty

                @endforelse

            </div>
            <div class="col-lg-12">
                <div class="primary-btn text-center">
                    <a href="#" class="btn-primary">Become Sponsor</a>
                </div>
            </div>
            <!-- /col end-->
        </div>
        <!-- /row end-->
    </div>
    <!-- /container end-->
</div>
<!--Our Sponsers Area End Here-->