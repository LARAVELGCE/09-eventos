<?php

namespace App\Listeners;

use App\Events\RegisteredUserEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailToCode
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisteredUserEvent  $event
     * @return void
     */
    public function handle(RegisteredUserEvent $event)
    {
        //
    }
}
