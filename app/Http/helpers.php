<?php
/**
 * Created by PhpStorm.
 * User: dev-dark
 * Date: 18/09/2018
 * Time: 8:47
 */
function setActiveRoute($name)
{
    return request()->routeIs($name) ? 'active' : '';
}
