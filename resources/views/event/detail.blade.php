@extends('layout')
@section('header-class','base-style-2')
@section('img','logo.png')

@section('content')
    @include('event.banner-count')
    @include('event.partials.about')
    @include('event.activity.index')
    @include('event.speaker.index')
    @include('event.sponsors.index')
    @include('event.partials.resumen')
    @include('event.partials.similar-event')

@stop
@push('script')
    <script src="/js/countdown.js"></script>
@endpush


