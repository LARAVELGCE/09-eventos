<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{

        public function searchCategory(Category $category)
    {
        // Estamos enviado a pantala del welcome solo las post de dicha categoria
        return view('events.home',
            [
                // Estamos enviado la categoria
                'category' => $category,
                // Estamos enviado a pantala del welcome solo las post de dicha categoria
                'events' => $category->events()->paginate(3)

            ]);

      //  return  $category->events()->paginate(3);
    }


}
