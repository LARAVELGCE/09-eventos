<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from sitetemplate.demo.ithemeslab.com/eventex/demo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 14 Sep 2018 22:21:16 GMT -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home</title>
    <meta name="author" content="iThemesLab">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicons -->
    <link rel="shortcut icon" href="/img/favicon/favicon.ico">
    <link rel="apple-touch-icon" href="/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">

    <!--Fuente-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700">
    <!--All Css Here-->
    <link rel="stylesheet" href="{{ mix('css/eventex.css') }}">

    <!--Modernizr Css-->
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>--}}


</head>

<body>


<!--Main Container Start Here-->
<div class="main-container">

    {{--si existe la session message --}}
    @if(session('message'))

        <div class="alert alert-{{ session('message')[0] }} adelante">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{--session()->flash('message', ['0', __("Inicio de sesión cancelado")]--}}
            <p>{{ session('message')[1] }}</p>

        </div>

    @endif
    @include('partials.nav')

    @yield('content')

   @include('partials.footer')

</div>
<!--Main Container End Here-->
<!--All Js Here-->
<script src="{{ mix('js/eventex.js') }}"></script>
@stack('script')
<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(3500, function(){
            $(this).remove();
        });
    }, 4000);
</script>

</body>


<!-- Mirrored from sitetemplate.demo.ithemeslab.com/eventex/demo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 14 Sep 2018 22:21:47 GMT -->
</html>
