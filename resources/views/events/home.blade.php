@extends('layout')
@section('img','logo-3.png')

@section('content')

    @include('partials.banner')
    <!--Blog Single Start Here-->
    <div class="blog-single pad100">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-8">
                    <!--Our Blog Area Start Here-->
                    <div class="our-blog-area">
                        @forelse($events as $event)

                        @if ($loop->iteration === 1)
                            <div class="single-blog xs-mb30">
                         @elseif ($loop->iteration === 2)
                                    <div class="single-blog">
                         @elseif ($loop->iteration === 3)
                                    <div class="single-blog mtb50">
                        @elseif ($loop->iteration === 4)
                                <div class="single-blog mtb50 mb0">
                         @endif

                            <div class="blog-img">
                                <img class="img-fluid" src="/images/events/{{ $event->photos->first()->picture }}" alt="">
                            </div>

                                <div class="blog-content">
                                    <a href="{{ route('events.show', $event) }}">
                                        <h3>{{ $event->title }}.</h3>
                                    </a>
                                    <p>{{ substr($event->description, 0,95)}}...</p>
                                    <div class="date">
                                        <ul>
                                            <li>
                                                {{ $event->start_at->format('d M Y') }}
                                                <span class="float-right"><i class="fa fa-share-alt"></i></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @empty
                        @endforelse


                        {{--<div class="pagination xs-mb30">
                            <ul>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">Next</a></li>
                            </ul>
                        </div>--}}
                    </div>
                    <!--Our Blog Area End Here-->
                                            {{ $events->links() }}
                </div>
                <!-- /.col end-->
                @include('events.partials.sidebar')
                <!-- /.col end-->
            </div>
            <!-- /.row end-->
        </div>
        <!-- /.container end-->
    </div>
    <!--Blog Single End Here-->

@stop


