<!--Contact Us Area Start Here-->
<div class="contact-us-area pad100 bg-6 parallax-2 no-attm">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="icon-box mb80">
                    <div class="box-icons">
                        <img src="/img/others/2.png" alt="">
                    </div>
                    <div class="inner-content">
                        <h3>Conference Location</h3>
                        <p>Washington State Convention Center 705 Pike Street ,Seattle, WA 98101 Phone: 206-694-5000</p>
                    </div>
                </div>
                <div class="icon-box mb80">
                    <div class="box-icons">
                        <img src="/img/others/3.png" alt="">
                    </div>
                    <div class="inner-content">
                        <h3>Hotel Information</h3>
                        <p>Sed minim feugiat quaestio ut. Erat singulist mei, eum reque ceteros omittantur ea. Cum facilisi molestiae</p>
                    </div>
                </div>
                <div class="icon-box">
                    <div class="box-icons">
                        <img src="/img/others/4.png" alt="">
                    </div>
                    <div class="inner-content">
                        <h3>Airport Information</h3>
                        <p>Washington State Convention Center 705 Pike Street ,Seattle, WA 98101 Phone: 206-694-5000</p>
                    </div>
                </div>
            </div>
            <!-- /col end-->
            <div class="col-lg-7">
                <div class="inner-contact xs-mt30">
                    <div class="section-title mb50">
                        <div class="title-text pl">
                            <h2>Registro</h2>
                        </div>
                    </div>
                    <form action="#">
                        <div class="row">
                            <div class="col-lg-12 mb50">
                                <input class="form-control" type="text" placeholder="Name" required>
                            </div>
                            <div class="col-lg-12 mb50">
                                <input class="form-control" type="email" placeholder="Email" required>
                            </div>
                            <div class="col-lg-12 mb50">
                                <input class="form-control" type="text" placeholder="Subject" required>
                            </div>
                            <div class="col-lg-12">
                                <textarea class="form-control" name="massage" placeholder="Massage"></textarea>
                            </div>
                            <div class="col-lg-12">
                                <button class="btn-primary xs-mt30" type="submit">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /col end-->
        </div>
        <!-- /row end-->
    </div>
    <!-- /container end-->
</div>
<!--Contact Us Area End Here-->
