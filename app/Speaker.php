<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Speaker
 *
 * @property int $id
 * @property string $name
 * @property string $job
 * @property string|null $picture
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Activity[] $activities
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Speaker whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Speaker whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Speaker whereJob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Speaker whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Speaker wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Speaker whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Speaker extends Model
{



    // cada speaker tiene muchos activitiees
    public function activities() {
        return $this->hasMany(Activity::class);
    }


    public function pathAttachment () {
        return "/images/speakers/" . $this->picture;
    }


}
