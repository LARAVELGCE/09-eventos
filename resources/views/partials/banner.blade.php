<!--Hero Banner Area Start Here-->
<div class="hero-banner-area hero-bg-4 parallax">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner-content">
                    <h1>Comunidad</h1>
                    <h2>Eventos 2018</h2>
                    <p>Enterate de las ultimos eventos a realizarse</p>
                    <div class="primary-btn">
                        <a href="#" class="btn-primary">Saber Mas</a>
                    </div>
                </div>
            </div>
            <!-- /col end-->
        </div>
        <!-- /row end-->
    </div>
    <!-- /container end-->
</div>
<!--Hero Banner Area End Here-->