<?php

namespace App\Events;

use App\Event;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RegisteredUserEvent
{
    use Dispatchable, SerializesModels;
    /**
     * @var Event
     */
    public $event;
    public $codigoqr;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    // por aqui a traves dek evento recibiremoas la info - 1.1 para mandarselo al
    // controlador oara que lo escuche y lo reciba en mi EventsController
    public function __construct(Event $event, $codigoqr)
    {
        //
        $this->event = $event;
        $this->codigoqr = $codigoqr;
    }

}
