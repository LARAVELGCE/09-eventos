@extends('layout')
@section('header-class','base-style-2')
@section('img','logo.png')


@section('content')
    <div class="hero-banner-area home-4  hero-bg-3 parallax no-attm">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-5 d-none d-lg-block">
                    <div class="banner-content">

                    </div>
                </div>
                <!-- /col end-->
                <div class="col-lg-4">
                    <div class="contact-box">
                        <div class="inner-content">
                            <h3>Crear Curso</h3>
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="row">

                                    <div class="col-lg-12">
                                        <input  class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" >
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="col-lg-12">
                                        <button class="btn-primary" type="submit">Login</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /col end-->
            </div>
            <!-- /row end-->
        </div>
        <!-- /container end-->
    </div>
@stop