<!--Hero Banner Area Start Here-->
<div class="hero-banner-area home-2 hero-bg-2 parallax no-attm">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner-content">
                    <div class="upcoming">
                        <span class="is-countdown"> </span>
                        <div data-countdown="{{ $event->start_at }}"></div>

                    </div>
                    <h3>{{ $event->title }}</h3>
                    <span>{{ $event->location }}</span>
                    @include('event.partials.button_action')

                </div>
            </div>
            <!-- /col end-->
        </div>
        <!-- /row end-->
    </div>
    <!-- /container end-->
</div>
<!--Hero Banner Area End Here-->
