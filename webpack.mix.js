const mix = require('laravel-mix');



/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.styles([
    'resources/eventex/css/bootstrap.min.css',
    'resources/eventex/css/font-awesome.min.css',
    'resources/eventex/css/flaticon.css',
    'resources/eventex/css/owl.carousel.min.css',
    'resources/eventex/css/owl.theme.default.min.css',
    'resources/eventex/css/animate.css',
    'resources/eventex/css/jquery-ui.min.css',
    'resources/eventex/css/ightbox.css',
    'resources/eventex/css/style.css',
    'resources/eventex/css/responsive.css',

], 'public/css/eventex.css')
.scripts([
    'resources/eventex/js/jquery-3.2.1.min.js',
    'resources/eventex/js/jquery-migrate.js',
    'resources/eventex/js/popper-1.12.9.min.js',
    'resources/eventex/js/bootstrap.min.js',
    'resources/eventex/js/owl.carousel.min.js',
    'resources/eventex/js/jquery.counterup.min.js',
    'resources/eventex/js/waypoints-jquery.js',
    'resources/eventex/js/lightbox.js',
    'resources/eventex/js/jquery.appear.js',
    'resources/eventex/js/jquery-ui.min.js',
    'resources/eventex/js/wow.min.js',
    'resources/eventex/js/plugins.js',
    'resources/eventex/js/main.js',
], 'public/js/eventex.js');

