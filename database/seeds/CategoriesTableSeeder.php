<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Conciertos',
            'Teatro',
            'Tecnologia',
            'Deporte',
            'Comicidad',
            'Festival Peru',
            'Festival EEUU',
            'Congreso',
            'Premiación',
            'Desfile'
        ];

        foreach ($names as $name) {
            $category = \App\Category::create(['name' => $name,'slug' =>  str_slug($name, '-')]);
        }
    }
}
