<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Sponsor
 *
 * @property int $id
 * @property int $event_id
 * @property string|null $picture
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Event $event
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sponsor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sponsor whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sponsor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sponsor wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sponsor whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Sponsor extends Model
{
    // Cada sponsor pertenece a un evento
    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function pathAttachment () {
        return "/images/sponsors/" . $this->picture;
    }
}
