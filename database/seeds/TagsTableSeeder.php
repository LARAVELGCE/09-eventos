<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Negocio',
            'Exito',
            'Reuniones',
            'Calidad',
            'Emprendimiento',
            'Satisfaccion',
        ];

        foreach ($names as $name) {
            $tag = \App\Tag::create(['name' => $name,'slug' =>  str_slug($name, '-')]);
        }
    }
}
