<!--Our Blog Area Start Here-->
<div class="our-blog-area bg-5 parallax pad100 no-s">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="section-title">
                    <div class="title-text pl">
                        <h2>Eventos Similares</h2>
                    </div>
                    <p>Lorem ipsum dolor sit amet, est suscipit epicurei recusabo ex. Feugait salutatus pertinacia vis ea, delenit democritum nam in, eos dolore legere insed corrumpit vituperatat.</p>
                </div>
            </div>
            <!-- /col end-->
            <div class="col-lg-8">
                <div class="blog-active owl-carousel owl-theme">
                    @forelse ($related as $relate)
                        <div class="single-blog xs-mt30">
                            <div class="blog-img">
                                <img class="img-fluid" src="/images/events/{{ $relate->photos->first()->picture }}" alt="">
                            </div>
                            <div class="blog-content">
                                <a href="{{ route('events.show', $relate) }}">
                                    <h3>{{$relate->title}}.</h3>
                                </a>
                                <p>{{$relate->description}}.</p>
                                <div class="date">
                                    <ul>
                                        <li>
                                            {{ $relate->start_at->format('d M Y') }}
                                            <span class="float-right"><i class="fa fa-share-alt"></i></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @empty

                        No hay eventos parecidos
                    @endforelse

                </div>
            </div>
            <!-- /col end-->
        </div>
        <!-- /row end-->
    </div>
    <!-- /container end-->
</div>
<!--Our Blog Area End Here-->