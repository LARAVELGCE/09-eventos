<?php

namespace App\Http\Controllers;

use App\Event;
use App\Events\RegisteredUserEvent;
use App\Mail\NewUserInEvent;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    public function show(Event $event)
    {

       $event->load(['category','tags','photos','activities','sponsors','activities.speaker'])->get();
       $related = $event->relatedEvents();

       return view('event.detail', compact('event', 'related'));

    }

    public function inscribe (Event $event) {

        $code = str_random(10);
        // dentro de la tabla course_student insertar el id del estudiante
          $event->users()->attach(auth()->user()->id,
              ['codigo' => $code]);
          // Envio de Email
        \Mail::to(auth()->user())->send(new NewUserInEvent($event,$code));
        return back()->with('message', ['success', __("Inscrito correctamente al evento")]);
    }

    public function create()
    {
        $event = new Event;
        $btnText = __("Crear Evento");
        return view('events.form',compact('event','btnText'));

    }


}
