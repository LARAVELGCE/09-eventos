<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Activity
 *
 * @property int $id
 * @property int $event_id
 * @property int $speaker_id
 * @property string $description
 * @property string|null $start_at
 * @property string|null $end_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Event $event
 * @property-read \App\Speaker $speaker
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereSpeakerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Activity extends Model
{

    // protected $withCount = ['speakers'];
    // para que laravel se de cuenta que tambien tiene formato de fecha
    protected $dates = ['start_at','end_at'];
    // Cada activity pertenece a un evento
    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    // Cada activity  tiene una solo speaker
    public function speaker()
    {
        return $this->belongsTo(Speaker::class);
    }

}
