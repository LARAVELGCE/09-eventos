<?php

use Faker\Generator as Faker;

$factory->define(App\Speaker::class, function (Faker $faker) {
    return [

        'name' =>  $faker->name,
        'job' =>  $faker->jobTitle,
        'picture' => \Faker\Provider\Image::image(storage_path() . '/app/public/speakers', 407, 407, 'men',false),

    ];
});
