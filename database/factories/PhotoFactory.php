<?php

use Faker\Generator as Faker;

$factory->define(App\Photo::class, function (Faker $faker) {
    return [
        'event_id' => \App\Event::all()->random()->id,
        'picture' => \Faker\Provider\Image::image(storage_path() . '/app/public/events', 370, 250, 'business',false),
    ];
});
