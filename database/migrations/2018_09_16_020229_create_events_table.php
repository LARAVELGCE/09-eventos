<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->mediumText('description')->nullable();
            $table->mediumText('location');
            $table->string('slug')->nullable();


            // Relacion con Categoria 1 a muchos
            $table->unsignedInteger('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on('categories');

            // Relacion con Categoria 1 a muchos
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');


            $table->timestamp('start_at')->nullable();

            $table->timestamps();
        });

        Schema::create('event_user', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger("user_id");
            $table->foreign("user_id")->references("id")->on("users");

            $table->unsignedInteger('event_id');
            $table->foreign('event_id')->references('id')->on('events');

            $table->string('codigo')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
