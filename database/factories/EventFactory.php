<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Event::class, function (Faker $faker) {

    $title = $faker->jobTitle;
    $startDate = Carbon::createFromTimeStamp($faker->dateTimeBetween('now', '+45 days')->getTimestamp());
    return [

        'title' => $title,
        'description' =>  $faker->text,
        'location' =>  $faker->address,
        'slug' => str_slug($title, '-'),
        'category_id' => \App\Category::all()->random()->id,
        'user_id' => \App\User::all()->random()->id,
        'start_at' =>  $startDate,

    ];
});
